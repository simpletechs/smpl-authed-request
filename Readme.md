# smpl-authed-request

A lighweight wrapper around `request-promise` to automatically make authorized requests to other services by obtaining a token from the `service-auth` service.

## Environment setup

The following environment variables are used by this module:

| Name                        | Value        | Required | Description                                          |
| --------------------------- | ------------ | -------- | ---------------------------------------------------- |
| `ADMIN_AUTH_OVERRIDE`       | JWT Token    | `false`  | Can be used in development to provide a static token |
| `SMPL_SERVICE_AUTH_API_URL` | `http://...` | `true`   | Specifies the endpoint used to acquire a new token   |

## Usage

Use this module just as you would use `request-promise`:

```js
const rp = require("request-promise");
try {
  const text = await rp("https://www.google.com");
} catch (ex) {}
```

**Note** that the first `HTTP Error 401` will cause `smpl-authed-request` to refresh the token stored internally and then retry the request. There is no indication that this happened, except that the second request _should_ work and from the outside it seems like there was no problem at all.
