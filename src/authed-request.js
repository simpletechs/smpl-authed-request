const request = require('request-promise');
const jwt = require('jsonwebtoken');
const log = require('debug')('src:authed-request');

let jwtAuthToken = undefined;
let authToken = '';

// handles authorized requests eg. to data-service
async function authedRequest(requestUrl, payload) {
    await updateAuthTokenIfNeeded();

    const { headers = {} } = payload;
    // add authHeader with current authToken to request
    Object.assign(payload, {
        headers: {
            ...headers,
            Authorization: 'Bearer ' + authToken
        }
    });

    return await retryOn401(() => request(requestUrl, payload));
}

async function retryOn401(fn) {
    return fn(authToken).catch(async function(err) {
        if (err.statusCode == 401) {
            await updateAuthTokenIfNeeded();
            return fn(authToken);
        } else {
            throw err;
        }
    });
}

// updates internal auth token if necessary
async function updateAuthTokenIfNeeded() {
    log('updateAuthTokenIfNeeded');
    if(process.env.ADMIN_AUTH_OVERRIDE) {
        authToken = process.env.ADMIN_AUTH_OVERRIDE;
        log('-- Using ADMIN_AUTH_OVERRIDE environment variable for authentication');
        return;
    }
    // if token has been decoded, does not expire in 60 seconds and is not expired already, do nothing
    // we use ms as a common unit for comparison
    if(jwtAuthToken && jwtAuthToken.payload && jwtAuthToken.payload.exp && jwtAuthToken.payload.exp * 1000 > Date.now() + 60 * 1000) {
        log('-- no refresh needed');
        return;
    }
    // else fetch new token from smpl auth service
    try {
        var res = await request(process.env.SMPL_SERVICE_AUTH_API_URL, {
            method: 'POST',
            json: true
        });
        if(res.token) {
            jwtAuthToken = jwt.decode(res.token, {complete: true}); // complete: true forces split in 'payload' and 'header'
            if(!jwtAuthToken) {
                throw new Error('unable to decode jwtToken');
            }
            authToken = res.token;
            log('-- refreshed token', authToken && authToken.length);
            return;
        }
        throw new Error('cannot get AuthToken');
    } catch(ex) {
        log(ex);
        throw ex;
    }
}

module.exports = authedRequest;