module.exports = {
    // parser: 'babel-eslint',
    env: {
        browser: false,
        node: true,
        es6: true
    },
    extends: ['eslint:recommended'],
    installedESLint: true,
    parserOptions: {
        ecmaVersion: 8,
        ecmaFeatures: {
            experimentalObjectRestSpread: true,
            async: true
        },
        sourceType: 'module'
    },
    plugins: [
    ],
    rules: {

        'no-console': 0,
        indent: [
            'warn',
            4,
            { 'SwitchCase': 1}
        ],
        'linebreak-style': [
            'warn',
            'unix'
        ],
        quotes: [
            'warn',
            'single'
        ],
        semi: [
            'warn',
            'always'
        ]
    }
};