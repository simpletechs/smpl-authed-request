const { expect } = require('chai');
const proxyquire = require('proxyquire');
const jwt = require('jsonwebtoken');
const spy = require('sinon').spy();

// define default and global response from service-auth, 24h valid token
const originalServiceAuthResponse = {token: jwt.sign({ exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24) }, 'shhh')};
let serviceAuthResponse = originalServiceAuthResponse;
let authedRequest; // holds our authed-request module

describe('src/authed-request.js', () => {

    before(() => {
    	// just to make sure
        expect(process.env).to.have.property('NODE_ENV', 'test');

        /*
         * set testable environment variables
         */
        // way to identify calls made by mocked request-promise to service-auth
        process.env.SMPL_SERVICE_AUTH_API_URL = 'createAuthToken';
    });

    beforeEach(() => {
        // re-proxyquire to reset internal state for each test (else we would not run into refresh function)
        spy.reset();
        authedRequest = proxyquire('../src/authed-request', {
            'request-promise': async function(requestedUrl, payload) {
                if(requestedUrl.indexOf('createAuthToken') > -1) {
                    // We received a call meant to reach the service-auth, let's return currently set response.
                    return serviceAuthResponse;
                } else {
                    // We received a call meant to reach another service
                    spy.apply(this, arguments);
                }
            }
        });
    });

    afterEach(() => {
        // reset process.env variables which might be altered during tests
        delete process.env.ADMIN_AUTH_OVERRIDE;
    });

    describe('service-auth token request', () => {
        it('should fail generating a token if the token cannot be decoded', async () => {
            serviceAuthResponse = {token: 'undecodable token from auth service'};
            try {
                await authedRequest('http://example.com', {});
                expect(false).to.be.true;
            } catch(ex) {
                expect(ex).to.be.instanceOf(Error);
                expect(ex.message).to.contain('unable to decode jwtToken');
            }
        });

        it('should use admin auth override if specified', async () => {
            process.env.ADMIN_AUTH_OVERRIDE = 'ADMIN_AUTH_OVERRIDE';

            try {
                // should not call service-auth at all
                await authedRequest('http://example.com', {});
                expect(spy.calledOnce).true;
                expect(spy.getCall(0).args[1]).to.have.nested.property('headers.Authorization').equals('Bearer ' + process.env.ADMIN_AUTH_OVERRIDE);
            } catch(ex) {
                expect(false).true;
            }
        });

        it('should generate valid token and not refresh on second request', async () => {
            // first default request
            serviceAuthResponse = originalServiceAuthResponse;
            await authedRequest('http://example.com', {});

            // check if a second request to the service-auth will be made - should not due to caching and unreached expiry of 1 day
            serviceAuthResponse = {token: 'newer invalid Token'};
            try {
                await authedRequest('http://example.com', {});
                expect(true).true;
            } catch(ex) {
                expect(false).true;
            }
        });

        it('should create a new token if token is close to expire', async () => {
            // expires in 30 seconds
            serviceAuthResponse = {token: jwt.sign({ test: 'expire', exp: Math.floor(Date.now() / 1000) + 30 }, 'shhh')};
            await authedRequest('http://example.com', {});

            // check if a second request to the service-auth will be made
            serviceAuthResponse = {token: jwt.sign({ test: 'expire', exp: Math.floor(Date.now() / 1000) }, 'shhh')};
            try {
                await authedRequest('http://example.com', {});
                expect(spy.calledTwice).true;
                expect(spy.getCall(1).args[1]).to.have.nested.property('headers.Authorization').equals('Bearer ' + serviceAuthResponse.token);
            } catch(ex) {
                expect(false).true;
            }
        });

        it('should create a new token if token is expired', async () => {
            // expired
            serviceAuthResponse = {token: jwt.sign({exp: Math.floor(Date.now() / 1000) - 30 }, 'shhh')};
            await authedRequest('http://example.com', {});

            // check if a second request to the service-auth will be made
            serviceAuthResponse = {token: jwt.sign({exp: Math.floor(Date.now() / 1000) }, 'shhh')};
            try {
                await authedRequest('http://example.com', {});
                expect(spy.calledTwice).true;
                expect(spy.getCall(1).args[1]).to.have.nested.property('headers.Authorization').equals('Bearer ' + serviceAuthResponse.token);
            } catch(ex) {
                expect(false).true;
            }
        });
    });

});